﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ammo : MonoBehaviour
{
    //script that generate a full magazin ready to use
    [SerializeField]
    GameObject prefabAmmo;
    [SerializeField]
    List<ammoBehavior> ammoReserve;
    [SerializeField]
    int ammoCount;
    public GameObject PrefabAmmo { get => prefabAmmo; set => prefabAmmo = value; }
    public List<ammoBehavior> AmmoReserve { get => ammoReserve; set => ammoReserve = value; }
    public int AmmoCount { get => ammoCount; set => ammoCount = value; }

    void Start()
    {
        AmmoReserve = new List<ammoBehavior>();
        FillMagazine();
    }

    private void FillMagazine()
    {
        if (AmmoReserve.Count <= 0)
            for (int i = 1; i < AmmoCount; i++)
            {
                GameObject temp = Instantiate(PrefabAmmo, transform.position, transform.rotation);
                AmmoReserve.Add(temp.GetComponent<ammoBehavior>());
            }
        else
        {
            foreach (var bullet in AmmoReserve)
            {
                AmmoReserve.Remove(bullet);
                Destroy(bullet.gameObject);
            }
            for (int i = 1; i < AmmoCount; i++)
            {
                GameObject temp = Instantiate(PrefabAmmo, transform.position, transform.rotation);
                AmmoReserve.Add(temp.GetComponent<ammoBehavior>());
            }
        }
    }

    public ammoBehavior GetFreeBullet()
    {
        ammoBehavior tempBullet = null;
        foreach (var potentialBullet in AmmoReserve)
        {
            if (!potentialBullet.HasBeenShot)
            {
                return potentialBullet;
            }
        }
        return tempBullet;
    }
    public void Reloading()
    {
        FillMagazine();
    }
}
