﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{ //script qui récupère une balle du chargeur et tir
    [SerializeField]
    ammo magazine;


    public ammo Magazine { get => magazine; set => magazine = value; }

 


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        ShootBullet();
        if (Input.GetKeyDown(KeyCode.R))
            ReloadMag();
    }
     
    private void ShootBullet()
    {

            GameObject temp = Magazine.GetFreeBullet().gameObject;
            temp.transform.localPosition = transform.position;
            temp.transform.localRotation = transform.rotation;
            temp.GetComponent<ammoBehavior>().GetShot();
        

    }
    private void ReloadMag()
    {
        Magazine.Reloading();
    }
}
