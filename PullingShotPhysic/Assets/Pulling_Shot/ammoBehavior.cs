﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ammoBehavior : MonoBehaviour
{
    //script de fonctionnement des munitions
    [SerializeField]
    float lifespan;
    [SerializeField]
    float speed;
    [SerializeField]
    float mass;
    [SerializeField]
    Rigidbody rb;
    [SerializeField]
    bool hasBeenShot;

    public float Lifespan { get => lifespan; set => lifespan = value; }
    public float Speed { get => speed; set => speed = value; }
    public float Mass { get => mass; set => mass = value; }
    public Rigidbody Rb { get => rb; set => rb = value; }
    public bool HasBeenShot { get => hasBeenShot; set => hasBeenShot = value; }

    void Start()
    {
        HasBeenShot = false;
        Rb = GetComponent<Rigidbody>();
        Rb.useGravity = false;
    }
    
    IEnumerator LifespanTrigger()
    {
        yield return new WaitForSeconds(Lifespan);
        gameObject.SetActive(false);

    }
    private void OnEnable()
    {
        HasBeenShot = false;
        Rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
    }
    public void GetShot()
    {
        Rb.AddForce(Vector3.Normalize(Vector3.forward+transform.position)*Speed, ForceMode.Impulse);
        Rb.useGravity = true;
        HasBeenShot = true;
        StartCoroutine("LifespanTrigger");
        
    }
   
}
