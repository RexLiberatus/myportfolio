﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sc_OpenWall : MonoBehaviour
{
    public Sc_ButtonConditionRoom scriptButton;

    [SerializeField]
    private float maxheight;
    [SerializeField]
    private float speed = 1.0f;
    [SerializeField]
    bool goDown;
    [SerializeField]
    bool startOpen;

    Vector3 initPos;

    public bool GoDown { get => goDown; set => goDown = value; }
    public bool StartOpen { get => startOpen; set => startOpen = value; }

    private void Start()
    {
        initPos = transform.position;
    }
 
    void Update()
    {
        if (scriptButton.CheckOpenDoor == true || StartOpen)
        {
            if (gameObject.transform.position.y <= maxheight)
            {
                transform.Translate(Vector3.up * speed * Time.deltaTime);
            }
            if (transform.position.y >= initPos.y && goDown)
            {
                transform.Translate(Vector3.up * -speed * Time.deltaTime);
            }
        }
        if (Sc_GlobalAlertManager.AlertState)
        {
            if (transform.position.y >= initPos.y)
            {
                transform.Translate(Vector3.up * -speed * Time.deltaTime);
            }
        }
    }
}
