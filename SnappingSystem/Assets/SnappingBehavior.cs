﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappingBehavior : MonoBehaviour
{
    [SerializeField]
    bool isConnected;
    [SerializeField]
    Transform referencePosition;

    public Transform ReferencePosition { get => referencePosition; set => referencePosition = value; }
    public bool IsConnected { get => isConnected; set => isConnected = value; }

    private void Start()
    {
        IsConnected = false;
    }
    private void Update()
    {
        if (IsConnected)
        {
            transform.position = ReferencePosition.position;
            transform.rotation = ReferencePosition.rotation;

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        SnappDetection sd = other.GetComponent<SnappDetection>();
        if (sd != null)
        {
            sd.IsSnapped = true;
            IsConnected = true;
            ReferencePosition = other.transform;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        SnappDetection sd = other.GetComponent<SnappDetection>();
        if (sd != null && IsConnected)
        {
            ReferencePosition = other.transform;
        }
    }
}
