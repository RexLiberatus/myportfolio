﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnappDetection : MonoBehaviour
{
    [SerializeField]
    bool isSnapped;
    [SerializeField]
    GameObject SnappedItem;
    public bool IsSnapped { get => isSnapped; set => isSnapped = value; }
    private void Start()
    {
        SnappedItem = new GameObject();
        IsSnapped = false;
    }
    private void Update()
    {
        if(!IsSnapped)
        {
            SnappingBehavior sb = SnappedItem.GetComponent<SnappingBehavior>();
            if ( sb!=null)
            sb.IsConnected = false;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        SnappingBehavior sb = other.GetComponent<SnappingBehavior>();
        if(sb!=null && !sb.IsConnected)
        {
            SnappedItem = sb.gameObject;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if( IsSnapped)
        {
            SnappingBehavior sb = other.GetComponent<SnappingBehavior>();
            if (sb != null && sb.IsConnected)
            {
                SnappedItem = sb.gameObject;
            }
        }
    }

}
