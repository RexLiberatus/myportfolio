﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{

    public float currHP = 30.0f;
    public float maxHP = 120.0f;

    public Texture2D hpBarTexture;
    public float hpBarLenght;
    public float PercentOfHP;
    bool detectDeath;


    // Start is called before the first frame update
    void OnGUI()
    {
        if (currHP > 0)
        {
            GUI.DrawTexture(new Rect(10, 10, hpBarLenght, 10), hpBarTexture);
        }
    }
    private void Start()
    {
        detectDeath = false;
    }

    // Update is called once per frame
    void Update()
    {
        PercentOfHP = currHP / maxHP;
        hpBarLenght = PercentOfHP * 100;

        if (currHP < maxHP)
        { 
        //currHP += Time.deltaTime * 0.5f;
        }
        if (currHP < 1 && !detectDeath)
        {
            detectDeath = true;
            CanvasManager.over.SetActive(true);
            //player.SetActive(false);
            //currHP = 30.0f ;
        }

        if (currHP > maxHP)
        {
            currHP = maxHP;
        }



    }
    void OnCollisionEnter2D(Collision2D enemy)
    {
        Debug.Log("Tu es dans l'ennemi");
        switch (enemy.gameObject.tag)
        {
            case "Enemy1":
                Debug.Log("testcase");
                currHP -= 1;
                break;
            case "Enemy2":
                currHP -= 1.5f;
                break;
            case "Enemy3":
                currHP -= 5;
                break;
            case "Boss":
                currHP -= 20;
                break;
            default:
                break;
        }




    }
}

