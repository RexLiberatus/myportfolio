﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class doorwarp : MonoBehaviour
{
    private bool timer = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player" && timer == true)//check if object is a player 
        {
            var testdoor = gameObject.tag;//catch the door tag
            if (testdoor == "DoorX")//work with horizontal doors
            {
                if (other.transform.position.x < gameObject.transform.position.x)//check is the player on the left ?
                {
                    other.transform.position += (new Vector3(gameObject.transform.localScale.x, 0));//position the player
                }
                else
                {
                    other.transform.position -= (Vector3.right + new Vector3(gameObject.transform.localScale.x, 0));
                }
            }
            else if (testdoor == "DoorY")//work with vertical doors
            {
                if (other.transform.position.y < gameObject.transform.position.y)
                {
                    other.transform.position += (Vector3.up + new Vector3(0, gameObject.transform.localScale.y));
                }
                else
                {
                    other.transform.position -= (Vector3.up + new Vector3(0, gameObject.transform.localScale.y));

                }
                timer = false;
                StartCoroutine(DelayDoor());
            }
        }
    }
    private IEnumerator DelayDoor()
    {
        Debug.Log("door closing");
        yield return new WaitForSeconds(2);
        Debug.Log("door closed");
        timer = true;

    }


}
