﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour
{
    public static GameObject win;
    public static GameObject over;

    // Start is called before the first frame update
    void Awake()
    {
        if (tag == "Win")
            win = gameObject;

        if (tag == "Over")
            over = gameObject;

        gameObject.SetActive(false);
    }

    public void Restart()
    {
        SceneManager.LoadScene("SampleScene");
        gameObject.SetActive(false);
    }
}
