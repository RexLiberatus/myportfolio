﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed = 3.0f;
    public float rSpeed = 100.0f;
    public Vector2 offset;
    bool doorKey = false;
    public GameObject weapon;
    public GameObject startC;

    Bounds currentRoomBounds;

    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        GeneralMove();
        Attack();

    }
    private void GeneralMove()
    {
        


        float horizontalInput = Input.GetAxis("Horizontal"); //Get user input
        float verticalInput = Input.GetAxis("Vertical"); // Get user input

        transform.Translate(Vector3.right * speed * horizontalInput * Time.deltaTime); //move horizontal
        transform.Translate(Vector3.up * speed * verticalInput * Time.deltaTime); //move  

        
        if (doorKey == false)
        { 
            transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x, currentRoomBounds.min.x + offset.x, currentRoomBounds.max.x - offset.x), Mathf.Clamp(gameObject.transform.position.y, currentRoomBounds.min.y + offset.y, currentRoomBounds.max.y - offset.y));
        }
      
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DoorY" || collision.tag=="DoorX")
        {
            doorKey = true;
            StartCoroutine(DelayDoor());
            weapon.SetActive(false);

        }
        if (collision.tag == "Room")
        {
            currentRoomBounds = collision.bounds;
        }
    }

    private IEnumerator DelayDoor()
    {
        Debug.Log("Il autorise la porte");
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        Debug.Log("Il passe la porte");
        doorKey = false;

    }

    Coroutine attackCoroutine;

    private void Attack()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            weapon.SetActive(true);

            if (attackCoroutine != null)
                StopCoroutine(attackCoroutine); //stop if already started

            attackCoroutine = StartCoroutine(DelayAttack());
            // weapon.SetActive(true);
        }


    }
    private IEnumerator DelayAttack()
    {
        yield return new WaitForSeconds(1f);
        weapon.SetActive(false);
    }
    public void setWeapon(string newweapon)
    {
        if (newweapon != null)
        {
            gameObject.GetComponentInChildren<Weapon_prop>().type = newweapon;
            gameObject.GetComponentInChildren<Weapon_prop>().DefineWeapon();
            startC.SetActive(false);
            weapon.SetActive(false);
        }
    }
}



