﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLimit : MonoBehaviour
{
    //bool doorKey = false;
    public Vector2 offset;
    Bounds currentRoomBounds;
    bool boundsChecked = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GeneralMove();
    }

    private void GeneralMove()
    {
        transform.position = new Vector3(Mathf.Clamp(gameObject.transform.position.x, currentRoomBounds.min.x + offset.x, currentRoomBounds.max.x - offset.x), Mathf.Clamp(gameObject.transform.position.y, currentRoomBounds.min.y + offset.y, currentRoomBounds.max.y - offset.y));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!boundsChecked)
        {
            if (collision.tag == "Room")
            {
                currentRoomBounds = collision.bounds;
                boundsChecked = true;
            }
        }
    }

    //private IEnumerator DelayDoor()
    //{
    //    Debug.Log("Il autorise la porte");
    //    yield return new WaitForFixedUpdate();
    //    yield return new WaitForFixedUpdate();
    //    Debug.Log("Il passe la porte");
    //    doorKey = false;

    //}
}
