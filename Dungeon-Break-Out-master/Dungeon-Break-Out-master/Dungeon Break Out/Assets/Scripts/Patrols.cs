﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrols : MonoBehaviour
{
    bool canPatrol = true;
    bool attack = false;
    public float speed = 5.0f;
    Vector3 target = new Vector3();
    Vector3 hitRange = new Vector3();
    Vector3 fightPosition = new Vector3();
    int destination = 1;
    float timer = 0f, trigger = 0.5f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= trigger)
        {
            if (canPatrol == true)
            {
                Patrol();

            }
            else
            {
                if (attack == false)
                {
                    transform.Translate((target / 2) * Time.deltaTime);

                }
                else
                {
                    Strike();
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.tag == "Player")
        {
            Debug.Log(gameObject.name + " attacks the player");
            canPatrol = false;
            fightPosition = other.gameObject.transform.position;
            target = (other.gameObject.transform.position - gameObject.transform.position);
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Debug.Log(gameObject.name + " back on patrol");
            canPatrol = true;
            attack = false;

        }
    }
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            attack = true;
            fightPosition = transform.position;
            Debug.Log(gameObject.name + " charged " + other.gameObject.tag);
        }
    }
    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player") { hitRange = other.gameObject.transform.position; }
    }
    private void Patrol()
    {
        switch (destination)
        {
            case 1:
                //go up
                transform.Translate(Vector3.right * speed * Time.deltaTime);

                break;
            case 2:
                //go dow
                transform.Translate(Vector3.left * speed * Time.deltaTime);

                break;
            case 3:
                //go left
                transform.Translate(Vector3.up * speed * Time.deltaTime);

                break;
            case 4:
                //go right
                transform.Translate(Vector3.down * speed * Time.deltaTime);

                break;
        }
        timer = 0;
        destination = (int)Random.Range(1, 4);
    }
    private void Strike()
    {
        Debug.Log("prepare next hit");
        StartCoroutine(Delay());

    }
    private IEnumerator Delay()
    {
        yield return new WaitForSeconds(1f);
        transform.Translate(-1 * Vector3.Normalize((hitRange- fightPosition)/2) * Time.deltaTime * speed);
        yield return new WaitForSeconds(1f);
        Debug.Log(gameObject.name + " hits the player");
        transform.Translate(Vector3.Normalize(hitRange - fightPosition) * Time.deltaTime * speed);
    }
}