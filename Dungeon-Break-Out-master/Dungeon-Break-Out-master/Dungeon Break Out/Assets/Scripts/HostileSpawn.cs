﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HostileSpawn : MonoBehaviour
{
    public int Difficulty = 0;
    Vector2 Limitation;
    Vector3 PositionRoom;
    float LimitX = 0;
    float LimitY = 0;
    public List <GameObject> prefabList;
    private Dictionary<string,GameObject>Prefabs;
    // Start is called before the first frame update
    void Start()
    {
        Prefabs = new Dictionary<string, GameObject>();
        DicoCompletion(prefabList);//remplis le dictionnaire

        StartCoroutine(Delay());

        Limitation = gameObject.transform.parent.localScale;

        PositionRoom = gameObject.transform.parent.position;

        LimitX =Limitation.x;

        LimitY =Limitation.y;

        Spawning(PositionRoom, LimitX, LimitY);

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void Spawning(Vector3 vroom, float xlim, float ylim)
    {
        int i = 0;
        switch (Difficulty)
        {
            case 0:
             Instantiate(Prefabs["Trader"],vroom+new Vector3(xlim+Random.Range(-0.9f,0.9f),ylim+Random.Range(-0.9f,0.9f)) , Quaternion.identity);
                break;
            case 1:
                i = (int)Random.Range(3, 12);
                while (i>0)
                {
                    Instantiate(Prefabs["Enemy1"], vroom + new Vector3(xlim + Random.Range(-0.9f, 0.9f), ylim + Random.Range(-0.9f, 0.9f)), Quaternion.identity);
                    i -= 1;
                }
                break;
            case 2:
                i = (int)Random.Range(3, 7);
                while (i > 0)
                {
                    Instantiate(Prefabs["Enemy2"], vroom + new Vector3(xlim + Random.Range(-0.9f, 0.9f), ylim + Random.Range(-0.9f, 0.9f)), Quaternion.identity);
                    i -= 1;
                }
                break;
            case 3:
                i = (int)Random.Range(3, 5);
                while (i > 0)
                {
                    Instantiate(Prefabs["Enemy3"], vroom + new Vector3(xlim + Random.Range(-0.9f, 0.9f), ylim + Random.Range(-0.9f, 0.9f)), Quaternion.identity);
                    i -= 1;
                }
                break;
            case 4:
               
                    Instantiate(Prefabs["Boss"], vroom , Quaternion.identity);
           
                break;
            case 5:


                break;
        }
    }
    private void DicoCompletion (List<GameObject>myList)
    {
        if (myList != null)
        {
            int count = 0;
            while (count < myList.Count)
            {
                Prefabs.Add(myList[count].name,myList[count]);
                count += 1;
            }
        }
    }
    private IEnumerator Delay()
    {
    
        yield return new WaitForSeconds(5);
        Debug.Log("door closed");

    }
}
