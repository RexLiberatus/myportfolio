﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyHeal : MonoBehaviour
{
    public int MaxHealth;
    public int CurrentHealt;
    private GameObject winnerScene;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.tag == "Boss")
        {
            winnerScene = CanvasManager.win;
        }

        DefindMonster();
        CurrentHealt = MaxHealth;

    }

    // Update is called once per frame
    void Update()
    {
        if (CurrentHealt < 1 )
        {
            if (gameObject.tag == "Boss")
            {
                Destroy(gameObject);
                winnerScene.SetActive(true);
            }
            else { Destroy(gameObject); } 
        }
    }
    void DefindMonster()
    {
        switch(gameObject.tag)
        {
            case "Enemy1":
                MaxHealth = 4;
                break;
            case "Enemy2":
                MaxHealth = 10;
                break;
            case "Enemy3":
                MaxHealth = 18;
                break;
            case "Boss":
                MaxHealth = 35;
                break;
            default:
                break;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Weapon")
        {
            switch(collision.gameObject.name)
            {
                case "Dagger":
                    CurrentHealt -= 2;
                    break;
                case "Axe":
                    CurrentHealt -= 6;
                    break;
                case "Spear":
                    CurrentHealt -= 4;
                    break;
                default:
               
                    break;
            }
        }
    }
}
