﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon_prop : MonoBehaviour
{
    float range;
    float degats;
    public string type;
    public GameObject prefab;
    // Start is called before the first frame update


    void Start()
    {
        DefineWeapon();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void DefineWeapon()
    {
        switch (type)
        {
            case "Dagger":
                gameObject.transform.localScale = new Vector3(0.03f, 0.2f, 1f);
                gameObject.name = "Dagger";

                degats = 2;
                break;
            case "Axe":
                gameObject.transform.localScale = new Vector3(0.03f, 0.4f, 1f);
                gameObject.name = "Axe";

                degats = 5;
                break;
            case "Spear":
                gameObject.transform.localScale = new Vector3(0.03f, 0.8f, 1f);
                gameObject.name = "Spear";

                degats = 3;
                break;
            default:
                gameObject.transform.localScale = Vector3.zero;

                break;
        }
    }
}